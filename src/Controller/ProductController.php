<?php


namespace App\Controller;


use App\Entity\Feedback;
use App\Entity\Product;
use App\Form\FeedbackType;
use App\Repository\FeedbackRepository;
use App\Repository\ProductRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractController
{
    /**
     * @var ProductRepository
     */
    private $repository;
    /**
     * @var ObjectManager
     */
    private $em;

    public function __construct(ProductRepository $repository, ObjectManager $em)
    {

        $this->repository = $repository;
        $this->em = $em;
    }

    /**
     * * @Route("/produit/{slug}-{id}",name="product.show", requirements={"slug":"[a-z0-9\-]*"})
     * @param Product $product
     * @param $slug
     * @param FeedbackRepository $feedbackRepository
     * @param Request $request
     * @return Response
     */
    public function show(Product $product, $slug, FeedbackRepository $feedbackRepository, Request $request): Response
    {

        if ($product->getSlug() !== $slug) {
            return $this->redirectToRoute('product.show', [
                'id' => $product->getId(),
                'slug' => $product->getSlug()
            ], 301);
        }

        $feedback = new Feedback();

        $form = $this->createForm(FeedbackType::class, $feedback);

        $average = $feedbackRepository->findProductAvgNote($product);

        return $this->render('product/show.html.twig',[
            'product'=>$product,
            'notes'=>$average,
            'current_menu'=>'home',
            'form'=>$form->createView()
        ]);
    }

}