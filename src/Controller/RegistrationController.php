<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegistrationController extends AbstractController
{
    /**
     * @Route("/register", name="registration.register")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param ObjectManager $em
     * @return Response
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder, ObjectManager $em): Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class,$user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);
            $user->setIsActive(true);

            $em->persist($user);
            $em->flush();

            $this->addFlash('success', 'Votre compte à bien été enregistré');

            return $this->redirectToRoute('home');
        }

        return $this->render('registration/register.html.twig', [
            'controller_name' => 'RegistrationController',
            'current_menu'=>'register',
            'title'=>'Inscription',
            'form'=>$form->createView()
        ]);
    }
}
