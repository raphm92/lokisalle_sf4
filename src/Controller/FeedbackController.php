<?php

namespace App\Controller;


use App\Entity\Feedback;
use App\Entity\Product;
use App\Form\FeedbackType;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FeedbackController extends AbstractController
{
    /**
     * @var ObjectManager
     */
    private $objectManager;

    public function __construct(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * @Route("/feedback/new/{id}", options={"expose"=true}, name="product.feedback.new")
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function new(Request $request, $id): Response
    {
        $feedback = new Feedback();

        $product = $this->objectManager->getRepository(Product::class)->find($id);

        $form = $this->createForm(FeedbackType::class,$feedback);
        $form->handleRequest($request);


        $feedback->setProduct($product);



        if($request->isXmlHttpRequest() && $request->isMethod('post') ){

            $feedback->setComment($request->request->get('comment'));
            $feedback->setNote($request->request->get('note'));
            // créer un nouvel objet
            // setter son contenu avec $request->request->get
            //le persister et le flusher
            $this->objectManager->persist($feedback);
            $this->objectManager->flush();
            $this->addFlash('success', "Merci pour votre commentaire");
        }

        if ($form->isSubmitted() && $form->isValid()){
            $this->objectManager->persist($feedback);
            $this->objectManager->flush();
            $this->addFlash('success', "Merci pour votre commentaire");

            return $this->redirectToRoute('product.show', [
                'id'=>$feedback->getProduct()->getId(),
                'slug'=>$feedback->getProduct()->getSlug()
            ]);
        }

        return $this->render('feedback/add.html.twig',[
            'form'=>$form->createView()
        ]);

    }

    //creer une nouvelle route pour le post en distant


}