<?php


namespace App\Controller;


use App\Repository\ProductRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @var ProductRepository
     */
    private $repository;

    public function __construct(ProductRepository $repository)
    {

        $this->repository = $repository;
    }

    /**
     * @Route("/", name="home")
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @return Response
     */
    public function index( PaginatorInterface $paginator, Request $request) : Response{


        $products = $paginator->paginate(
            $this->repository->findAll(),
            $request->query->getInt('page', 1),
            12

        );

        return $this->render('pages/home.html.twig',[
            'products'=>$products,
            'current_menu'=>'home'
        ]);
    }
}