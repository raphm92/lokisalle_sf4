<?php


namespace App\Controller\Admin;


use App\Entity\Product;
use App\Form\ProductType;
use App\Repository\ProductRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminProductController extends AbstractController
{
    /**
     * @var ProductRepository
     */
    private $repository;
    /**
     * @var ObjectManager
     */
    private $em;

    public function __construct(ProductRepository $repository, ObjectManager $em)
    {

        $this->repository = $repository;
        $this->em = $em;
    }

    /**
     * @Route("/admin", name="admin.product.index")
     * @return Response
     */
    public function index(): Response
    {
        $product = $this->repository->findAll();
        return $this->render('admin/product/index.html.twig',[
            'products'=>$product,
            'current_menu'=>'admin'
        ]);
    }


    /**
     * @Route("/admin/product/add", name="admin.product.new")
     * @param Request $request
     * @return Response
     */
    public function add(Request $request): Response
    {
        $product = new Product();
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $this->em->persist($product);
            $this->em->flush();
            $this->addFlash('success','Le produit à été créé avec succès');
            return $this->redirectToRoute('admin.product.index');
        }

        return $this->render('admin/product/add.html.twig',[
            'form'=>$form->createView()
        ]);

    }

    /**
     * @Route("/admin/product/edit/{id}", name="admin.product.edit")
     * @param Product $product
     * @param Request $request
     * @return Response
     */
    public function edit(Product $product, Request $request): Response
    {
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $this->em->flush();
            $this->addFlash('success', 'Le produit a bien été mis à jour');
            return $this->redirectToRoute('admin.product.index');
        }

        return $this->render('admin/product/edit.html.twig',[
            'product'=>$product,
            'form'=>$form->createView()

        ]);
    }

    /**
     * @Route("/admin/product/{id}", name="admin.product.delete", methods="DELETE")
     * @param Product $product
     * @param Request $request
     * @return Response
     */
    public function delete(Product $product, Request $request): Response{
        if($this->isCsrfTokenValid('delete' . $product->getId(), $request->get('_token'))){
            $this->em->remove($product);
            $this->em->flush();
            $this->addFlash('success','Produit supprimé avec succes');
            return $this->redirectToRoute('admin.product.index');
        }
    }
}