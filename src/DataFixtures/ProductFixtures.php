<?php

namespace App\DataFixtures;

use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class ProductFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {

        $faker = Factory::create('fr_FR');

        for ($i=0; $i<100; $i++){
        $product = new Product();

        $product
            ->setTitle($faker->words(3, true))
            ->setDescription($faker->sentence(17))
            ->setPrice($faker->numberBetween(100,600))
            ->setCapacity($faker->numberBetween(1,40))
            ->setAddress($faker->streetAddress)
            ;

        $manager->persist($product);
        }
        $manager->flush();
    }
}
